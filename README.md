# Dotfiles

This page is still under construction and will be updated soon

## Envirionemnt setup

This section is still under construction and will be updated eventually as I migrate from OSX to Linux.

| Terminal                 | [Alacritty](https://github.com/alacritty/alacritty) |
| :----------------------- | :-------------------------------------------------- |
| **OS**                   | **[Arch Linux](https://archlinux.org/)**            |
| **WM**                   | **[i3](https://github.com/i3/i3)**                  |
| **Status bar**           | **[polybar](https://github.com/polybar/polybar)**   |
| **Application launcher** | **[rofi](https://github.com/davatorium/rofi)**      |
| **File manager**         | **[ranger](https://github.com/jarun/nnn)**          |
| **Editor**               | **[neovim](https://github.com/neovim/neovim)**      |
| **Terminal multiplexer** | **[tmux](https://github.com/tmux/tmux.git)**        |
| **Shell**                | **[fish](https://github.com/topics/fish)**          |
| **Automation**           | **[Hammerspoon](https://www.hammerspoon.org/)**     |

## ScreenShots

This section is still under construction and will be updated soon.

## Installation

To install the environment configuration for the project, you must run the following command:

```bash
$ ./install.sh
```

## Todo

- [ ] Migrate from OSX to Arch Linux definetely
- [ ] Migrate from Polybar to [EWW](https://github.com/elkowar/eww)
