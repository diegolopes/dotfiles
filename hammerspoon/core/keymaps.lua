local mod = { "alt" }

hs.hotkey.bind(mod, "b", open("Google Chrome"))
hs.hotkey.bind(mod, "t", open("Alacritty"))
hs.hotkey.bind(mod, "n", open("Notion"))
hs.hotkey.bind(mod, "o", open("Obsidian"))
hs.hotkey.bind(mod, "g", open("Gather"))
hs.hotkey.bind(mod, "m", open("Thunderbird"))
hs.hotkey.bind(mod, "c", open("Calendar"))
