#!/bin/bash
declare -a dotfiles

# List of dotfiles supported by the current installation script

dotfiles=(
  "nvim::$HOME/.config/nvim"
  "alacritty::$HOME/.config/alacritty"
  "tmux::$HOME/.config/tmux"
  "hammerspoon::$HOME/.hammerspoon"
)


for i in "${dotfiles[@]}"
do 
  dotfile="${i%%::*}"
  filepath="${i##*::}"
  
  rm -rf ${filepath} 
  cp -R ./${dotfile} ${filepath} 
done
