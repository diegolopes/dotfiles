<h1 align="center">Neovim</h1>

<div align="center">
  <a href="#install">Install</a>
  <span> • </span>
  <a href="#overview">Overview</a>
  <p></p>
</div>

<div align="center">
	
![Lua](https://img.shields.io/badge/Made%20with%20Lua-blueviolet.svg?style=for-the-badge&logo=lua)

</div>

Welcome to a personal Neovim configuration! This project aims to show everyone what a Lua configuration for Neovim looks like.
Feel free to open issues if you have suggestions or improvements. This is an ongoing project and I am constantly making
changes and experimenting new things.

# Install

**Prerequisites**

- Neovim 0.7+

Steps:

1. Clone the project in your config folder, usually `~/.config/nvim`:

```bash
$ git clone git@github.com:imdiegolopes/dotfiles.git
$ cd dotfiles && mv ./nvim ~/.config/nvim
```

2. Open Neovim. You should see a _"Downloading packer.."_ and then _"plugins installed"_ message in the first run

# Overview

## Plugins

Below the list of the current plugins used in this configuration and how we are using them

### Package Manager

- [packer](https://github.com/wbthomason/packer.nvim") - Package Managemente for NeoVIm written in Lua

### Visual

- [dracula](https://github.com/Mofiqul/dracula.nvim) - Main colorscheme
- [nvim-web-devicons](https://github.com/kyazdani42/nvim-web-devicons) - web devicons for general usage. Used in buffer tabs, statusline and telescope
- [lualine.nvim](https://github.com/shadmansaleh/lualine.nvim) - A blazing fast and easy to configure Neovim statusline written in Lua.
- [bufferline.nvim](https://github.com/akinsho/bufferline.nvim) - A snazzy 💅 buffer line (with tabpage integration) for Neovim built using lua.
- [colorizer.lua](https://github.com/norcalli/nvim-colorizer.lua) - A high-performance color highlighter for Neovim which has no external dependencies! Written in performant Luajit.

### LSP

- [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig) - Nvim supports the Language Server Protocol (LSP), which means it acts as a client to LSP servers and includes a Lua framework `vim.lsp` for building enhanced LSP tools.
- [nvim-lsp-installer](https://github.com/williamboman/nvim-lsp-installer/) - Neovim plugin that allow you to manage LSP servers (servers are installed inside :echo stdpath("data") by default).
- [null-ls.nvim](https://github.com/jose-elias-alvarez/null-ls.nvim) - Use Neovim as a language server to inject LSP diagnostics, code actions, and more via Lua
- [lspkind.nvim](https://github.com/onsails/lspkind.nvim) - This tiny plugin adds vscode-like pictograms to neovim built-in lsp

### Git

- [lazygit](https://github.com/kdheepak/lazygit.nvim) - Plugin for calling lazygit within NeoVIM.
- [gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim) - Show git signs in buffer
- [blamer.nvim](https://github.com/APZelos/blamer.nvim) - A git blame plugin for neovim inspired by VS Code's GitLans plugin.

### Snippets

- [LuaSnip](https://github.com/sar/luasnip.nvim) - Snippeter Engine for Neovim written in Lua
- [Friendly Snippets](https://github.com/rafamadriz/friendly-snippets) - Snippets collection for a set of different programming languages for faster development.

### Code Completion

- [nvim-cmp](https://github.com/hrsh7th/nvim-cmp) - A completion engine plugin for neovim written in Lua. Completion sources are installed from external repositories and "sourced".
- [cmp-buffer](https://github.com/sar/cmp-buffer.nvim) - nvim-cmp source for buffer words.
- [cmp_luasnip](https://github.com/saadparwaiz1/cmp_luasnip) - luasnip completion source for nvim-cmp
- [nvim-path](https://github.com/hrsh7th/cmp-path) - nvim-cmp source for filesystem paths.
- [cmp-nvim-lsp](https://github.com/hrsh7th/cmp-nvim-lsp) - nvim-cmp source for neovim's built-in language server client.
- [cmp-nvim-lua](https://github.com/hrsh7th/cmp-nvim-lua) - nvim-cmp source for neovim Lua API.
- [cmp-cmdline](https://github.com/hrsh7th/cmp-cmdline) - nvim-cmp source for vim's cmdline.
- [cmp-tabnine](https://github.com/tzachar/cmp-tabnine) - Tabnine source for hrsh7th/nvim-cmp

### Code Tests

- [neotest](https://github.com/nvim-neotest/neotest) - An extensible framework for interacting with tests within NeoVim.

### Others

- [Comment](https://github.com/numToStr/Comment.nvim) - Shortcut for commenting in and out code snippets
- [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter) - Used on syntax highlight and custom motions
- [ctrlsf](https://github.com/dyng/ctrlsf.vim) - An ack/ag/pt/rg powered code search and view tool, takes advantage of Vim 8's power to support asynchronous searching, and lets you edit file in-place with Edit Mode.
- [which-key](https://github.com/folke/which-key.nvim/) - WhichKey is a lua plugin for Neovim 0.5 that displays a popup with possible key bindings of the command you started typing
- [nvim-ts-autotag](https://github.com/windwp/nvim-ts-autotag) - Use treesitter to autoclose and autorename HTML tag.
- [indent blankline](https://github.com/lukas-reineke/indent-blankline.nvim) - Adds indentation guides to all lines, including empty lines
- [telescope.nvim](https://github.com/nvim-telescope/telescope.nvim) - telescope.nvim is a highly extendable fuzzy finder over lists. Built on the latest awesome features from neovim core. Telescope is centered around modularity, allowing for easy customization.
- [telescope-file-browser.nvim](https://github.com/nvim-telescope/telescope-file-browser.nvim) - telescope-file-browser.nvim is a file browser extension for telescope.nvim. It supports synchronized creation, deletion, renaming, and moving of files and folders powered by telescope.nvim and plenary.nvim.
- [telescope-file-browser.nvim](https://github.com/nvim-telescope/telescope-file-browser.nvim) - telescope-file-browser.nvim is a file browser extension for telescope.nvim. It supports synchronized creation, deletion, renaming, and moving of files and folders powered by telescope.nvim and plenary.nvim.
- [telescope.nvim](https://github.com/nvim-lua/telescope.nvim) - Find, filter, preview and pick using a nice UI

### Plugin development

- [plenary.nvim](https://github.com/nvim-lua/plenary.nvim) - Lua helpers for general usage

## Screenshots

![](https://i.postimg.cc/BZM9b68J/Screen-Shot-2022-09-19-at-9-50-10-PM.png)

### Get healthy

Open nvim and enter the following:

```
:checkhealth
```

### Todo

- [ ] Install the [nvim-dap](http://neovimcraft.com/plugin/mfussenegger/nvim-dap/index.html) which is a Debug Adapter Protocol client implementation for Nveovim, that allows to debug an application.
- [ ] Replace the [nvim-lsp-installer]() plugin by the [Mason]() plugin due to the first plugin is no longer maintained by the community
