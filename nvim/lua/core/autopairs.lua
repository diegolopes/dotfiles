-- A super powerful autopair plugin for Neovim that supports multiple characters.--

require("nvim-autopairs").setup({
	disable_filetype = { "TelescopePrompt", "vim" },
})
