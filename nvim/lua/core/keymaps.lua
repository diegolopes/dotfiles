local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",
--

-- Better window navigation
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- Move text up and down
keymap("n", "<A-j>", "<Esc>:m .+1<CR>==gi", opts)
keymap("n", "<A-k>", "<Esc>:m .-2<CR>==gi", opts)

-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Move text up and down
keymap("v", "<A-j>", ":m .+1<CR>==", opts)
keymap("v", "<A-k>", ":m .-2<CR>==", opts)
keymap("v", "p", '"_dP', opts)

-- Visual Block --
-- Move text up and down
keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)

-- Save

keymap("n", "<C-s>", "<cmd> w <CR>", opts)

-- Copy all

keymap("n", "<C-a>", "<cmd> %y+ <CR>", opts)

--Nvim Tree Toggle
keymap("n", "<c-n>", "<cmd>NvimTreeToggle<CR>", opts)
keymap("n", "<c-m>", "<cmd>NvimTreeFocus<CR>", opts)
keymap("n", "<c-k>", "<cmd>NvimTreeFindFile<CR>", opts)
keymap("n", "<c-c>", "<cmd>NvimTreeCollapse<CR>", opts)

-- Telescope
keymap("n", "<c-f>", "<cmd>Telescope find_files<cr>", opts)
keymap("n", "<Space>ff", "<cmd>Telescope find_files<cr>", opts)
keymap("n", "<Space>fg", "<cmd>Telescope live_grep<cr>", opts)
keymap("n", "<Space>fh", "<cmd>Telescope help_tags<cr>", opts)
keymap("n", "<Space>fb", "<cmd>Telescope file_browser<cr>", opts)
keymap("n", "<Space>fp", "<cmd>Telescope project<cr>", opts)

-- BlamerToggle
keymap("n", "<Space>bt", "<cmd>BlamerToggle<cr>", opts)

-- Trouble
keymap("n", "<Space>xx", "<cmd>TroubleToggle<cr>", opts)
keymap("n", "<Space>xw", "<cmd>TroubleToggle workspace_diagnostics<cr>", opts)
keymap("n", "<Space>xd", "<cmd>TroubleToggle document_diagnostics<cr>", opts)
keymap("n", "<Space>xq", "<cmd>TroubleToggle quickfix<cr>", opts)
keymap("n", "<Space>xl", "<cmd>TroubleToggle loclist<cr>", opts)
keymap("n", "gR", "<cmd>TroubleToggle lsp_references<cr>", opts)

-- Bufferline
keymap("n", "<S-Tab>", "<Cmd>BufferLineCyclePrev<CR>", opts)
keymap("n", "<Tab>", "<Cmd>BufferLineCycleNext<CR>", opts)
keymap("n", "<A-<>", "<Cmd>BufferLineMovePrevious<CR>", opts)
keymap("n", "<A->>", "<Cmd>BufferLineMoveNext<CR>", opts)
keymap("n", "<Space>1", "<cmd>BufferLineGoToBuffer 1<CR>", opts)
keymap("n", "<Space>2", "<cmd>BufferLineGoToBuffer 2<cr>", opts)
keymap("n", "<Space>3", "<cmd>BufferLineGoToBuffer 3<cr>", opts)
keymap("n", "<Space>4", "<cmd>BufferLineGoToBuffer 4<cr>", opts)
keymap("n", "<Space>5", "<cmd>BufferLineGoToBuffer 5<cr>", opts)
keymap("n", "<Space>6", "<cmd>BufferLineGoToBuffer 6<cr>", opts)
keymap("n", "<Space>7", "<cmd>BufferLineGoToBuffer 7<cr>", opts)
keymap("n", "<Space>8", "<cmd>BufferLineGoToBuffer 8<cr>", opts)
keymap("n", "<Space>9", "<cmd>BufferLineGoToBuffer 9<cr>", opts)
keymap("n", "<Space>c", "<cmd>BufferLineCloseLeft<cr>", opts)
keymap("n", "<Space>w", "<cmd>bdelete!<cr>", opts)
keymap("n", "<Space>bb", "<cmd>BufferOrderByBufferNumber<cr>", opts)
keymap("n", "<Space>bd", "<cmd>BufferOrderByDirectory<cr>", opts)
keymap("n", "<Space>bl", "<cmd>BufferOrderByLanguage<cr>", opts)
keymap("n", "<Space>bw", "<cmd>BufferOrderByWindowNumber<cr>", opts)

-- LazyGit
keymap("n", "<Space>gg", "<cmd>LazyGit<cr>", opts)

-- CTRL+Shift+F
keymap("n", "<Space>sf", "<Plug>CtrlSFPrompt", opts)
keymap("n", "<Space>sfo", "<cmd>CtrlSFOpen", opts)
keymap("n", "<Space>sft", "<cmd>CtrlSFToggle", opts)

-- Documentation, LSP & Reference
keymap("n", "gd", ":lua vim.lsp.buf.definition()<CR>", opts)
keymap("n", "gD", ":lua vim.lsp.buf.declaration()<CR>", opts)
keymap("n", "gi", ":lua vim.lsp.buf.implementation()<CR>", opts)
keymap("n", "gw", ":lua vim.lsp.buf.document_symbol()<CR>", opts)
keymap("n", "gW", ":lua vim.lsp.buf.workspace_symbol()<CR>", opts)
keymap("n", "gr", ":lua vim.lsp.buf.references()<CR>", opts)
keymap("n", "gt", ":lua vim.lsp.buf.type_definition()<CR>", opts)
keymap("n", "K", ":lua vim.lsp.buf.hover()<CR>", opts)
keymap("n", "gh", ":lua vim.lsp.buf.signature_help()<CR>", opts)
keymap("n", "<space>af", ":lua vim.lsp.buf.code_action()<CR>", opts)
keymap("n", "<space>rn", ":lua vim.lsp.buf.rename()", opts)
keymap("n", "<space>ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
keymap("n", "<space>e", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
keymap("n", "[d", '<cmd>lua vim.diagnostic.goto_prev({ border = "rounded" })<CR>', opts)
keymap("n", "gl", '<cmd>lua vim.diagnostic.open_float({ border = "rounded" })<CR>', opts)
keymap("n", "]d", '<cmd>lua vim.diagnostic.goto_next({ border = "rounded" })<CR>', opts)
keymap("n", "<leader>q", "<cmd>lua vim.diagnostic.setloclist()<CR>", opts)

-- Which Keys
--
keymap("n", "<space>", "<cmd>WhichKey ", opts)
