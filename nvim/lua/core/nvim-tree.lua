local status_ok, nvimtree = pcall(require, "nvim-tree")

if not status_ok then
	return
end

nvimtree.setup({
	auto_reload_on_write = true,
	create_in_closed_folder = false,
	hijack_cursor = false,
	hijack_netrw = true,
	open_on_setup = false,
	open_on_setup_file = false,
	open_on_tab = false,
	ignore_ft_on_setup = {
		"startify",
		"dashboard",
		"alpha",
	},
	view = {
		adaptive_size = true,
	},
})
