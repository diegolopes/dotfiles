-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
	return
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

local ensure_packer = function()
	local fn = vim.fn
	local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
	if fn.empty(fn.glob(install_path)) > 0 then
		fn.system({
			"git",
			"clone",
			"--depth",
			"1",
			"https://github.com/wbthomason/packer.nvim",
			install_path,
		})
		vim.cmd([[packadd packer.nvim]])
		return true
	end
	return false
end

local packer_bootstrap = ensure_packer()

return require("packer").startup(function(use)
	-- General
	use({ "wbthomason/packer.nvim" })
	use({ "akinsho/toggleterm.nvim", tag = "*" })
	use({ "kyazdani42/nvim-web-devicons" })
	use({ "kyazdani42/nvim-tree.lua" })
	use({ "windwp/nvim-autopairs" })
	use({
		"goolord/alpha-nvim",
		requires = { "kyazdani42/nvim-web-devicons" },
	})
	use({ "lewis6991/gitsigns.nvim", tag = "release" })
	use({ "nvim-lua/plenary.nvim" })
	use({ "numToStr/Comment.nvim" })
	use({
		"ahmedkhalf/project.nvim",
		commit = "541115e762764bc44d7d3bf501b6e367842d3d4f",
	})
	use({
		"lewis6991/impatient.nvim",
		commit = "969f2c5c90457612c09cf2a13fee1adaa986d350",
	})
	use({ "dyng/ctrlsf.vim" })
	use({ "folke/which-key.nvim" })
	-- use {"mg979/vim-visual-multi"}
	-- Telescope
	use({ "nvim-telescope/telescope.nvim", tag = "0.1.*" })
	use({ "nvim-telescope/telescope-file-browser.nvim" })
	use({ "nvim-telescope/telescope-project.nvim" })
	-- Git
	use({ "kdheepak/lazygit.nvim" })
	use({ "APZelos/blamer.nvim" })
	-- Treesitter
	use({
		"nvim-treesitter/nvim-treesitter",
		run = function()
			require("nvim-treesitter.install").update({
				with_sync = true,
			})
		end,
	})
	use({ "windwp/nvim-ts-autotag" })
	use({
		"akinsho/bufferline.nvim",
		tag = "v2.*",
		requires = "kyazdani42/nvim-web-devicons",
	})
	use({
		"nvim-lualine/lualine.nvim",
		requires = {
			"kyazdani42/nvim-web-devicons",
			opt = true,
		},
	})
	use("lukas-reineke/indent-blankline.nvim")
	-- LSP
	use({ "neovim/nvim-lspconfig", tag = "*" }) -- enable LSP
	use({ "williamboman/nvim-lsp-installer", tag = "*" }) -- simple to use language server installer
	use({ "jose-elias-alvarez/null-ls.nvim", tag = "*" }) -- for formatters and linters
	use({ "ray-x/go.nvim" })
	use({ "ray-x/guihua.lua" }) -- recommanded if need floating window support
	use({ "onsails/lspkind.nvim" })

	-- snippets
	use({ "L3MON4D3/LuaSnip" }) --snippet engine
	use({ "rafamadriz/friendly-snippets" }) -- a bunch of snippets to use

	-- CMP plugins"
	use({ "hrsh7th/nvim-cmp" }) -- The completion plugin
	use({ "hrsh7th/cmp-buffer" }) -- buffer completions
	use({ "hrsh7th/cmp-path" }) -- path completions
	use({ "saadparwaiz1/cmp_luasnip" }) -- snippet completions
	use({ "hrsh7th/cmp-nvim-lsp" })
	use({ "hrsh7th/cmp-nvim-lua" })
	use({ "hrsh7th/cmp-cmdline" })
	use({
		"tzachar/cmp-tabnine",
		run = "./install.sh",
		requires = "hrsh7th/nvim-cmp",
	})

	-- Colorize
	use({ "norcalli/nvim-colorizer.lua" })

	-- Colorscheme
	use({ "Mofiqul/dracula.nvim" })

	-- Automatically set up your configuration after cloning packer.nvim
	-- Put this at the end after all plugins
	if packer_bootstrap then
		require("packer").sync()
	end
end)
