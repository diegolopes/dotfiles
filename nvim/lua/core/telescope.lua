require("telescope").setup({
	defaults = {
		file_ignore_patterns = {
			"node_modules",
			"vendor",
			"tmp",
			"dist",
			".git",
			".vscode",
			".DS_Store",
		},
	},
})
require("telescope").load_extension("file_browser")
require("telescope").load_extension("project")
require("telescope").load_extension("lazygit")
